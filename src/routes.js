import { Switch, Route } from "react-router-dom";

import React from "react";
import Main from "./pages/Main/main";
import Cart from "./pages/Cart/index";
import Home from "./pages/Home/index";
import Payment from "./pages/Payment/index";

// import { Container } from './styles';

export default function Routes() {
  return (
    <Switch>
      <Route path="/payment" component={Payment} />

      <Route path="/" exact component={Main} />
      <Route path="/cart" component={Cart} />
      <Route path="/:albumId" component={Home} />
    </Switch>
  );
}
