
import React, { Component } from 'react';

import { FaCameraRetro, FaCheck } from "react-icons/fa";

 import { Container, SubmitButton, Form } from './styles';

export default class Main extends Component {
  render() {
    return (
     <Container>
       <h1>
         <FaCameraRetro />
         Álbuns
       </h1>

       <Form onSubmit={() => {}}>
         <input
          type="text"
          placeholder="Digite o código do album que deseja buscar" />
      <SubmitButton>
      <FaCheck color="#FFF" size={14} />
      </SubmitButton>
       </Form>


     </Container>
    );
  }
}
