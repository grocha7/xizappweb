/* eslint-disable react/prefer-stateless-function */
import React, { Component } from "react";

import { connect } from "react-redux";

import { MdAddShoppingCart } from "react-icons/md";

import { ProductList } from "./styles";
import api from "../../services/api";

class Home extends Component {
  state = {
    photos: []
  };

  async componentDidMount() {
    try {
      const response = await api.get(
        `api/photos/${this.props.match.params.albumId}`
      );
      const { photos } = response.data;
      console.log(this.props.match);

      this.setState({ photos });
    } catch (err) {
      console.log(err);
    }
  }

  handleAddPhoto = photo => {
    const { dispatch } = this.props;

    dispatch({
      type: "ADD_TO_CART",
      photo
    });
  };

  render() {
    return (
      <ProductList>
        {this.state.photos.map(photo => {
          return (
            <li key={photo.name}>
              <img
                src={`http://159.89.88.60:4000/images/${photo.name}`}
                alt={photo.name}
              />
              <strong>Foto 1</strong>
              <span>R$9,90</span>

              <button type="button" onClick={() => this.handleAddPhoto(photo)}>
                <div>
                  <MdAddShoppingCart size={16} color="#FFF" />
                </div>
                <span>ADICIONAR AO CARRINHO</span>
              </button>
            </li>
          );
        })}
      </ProductList>
    );
  }
}

export default connect()(Home);
