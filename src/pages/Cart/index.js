import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { MdDelete } from "react-icons/md";
import { Container, ProductTable, Total } from "./styles";

function Cart({ cart, dispatch, cartSize }) {
  return (
    <Container>
      <ProductTable>
        <thead>
          <tr>
            <th />
            <th>PRODUTO</th>

            <th />
          </tr>
        </thead>
        <tbody>
          {cart.map(photo => (
            <tr>
              <td>
                <img
                  src={`http://159.89.88.60:4000/images/${photo.name}`}
                  alt="fabiano"
                />
              </td>
              <td>
                <span>R$9,90</span>
              </td>

              <td>
                <div>
                  <button
                    type="button"
                    onClick={() =>
                      dispatch({
                        type: "REMOVE_FROM_CART",
                        id: photo.id
                      })
                    }
                  >
                    <MdDelete size={20} color="#F7941D" />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </ProductTable>

      <footer>
        <Link to="/payment">
          <button type="button">Finalizar pedido</button>
        </Link>

        <Total>
          <span>TOTAL</span>
          <strong>R$ {parseFloat(cartSize * 9.9).toFixed(2)}</strong>
        </Total>
      </footer>
    </Container>
  );
}

const mapStateToProps = state => ({
  cart: state.cart,
  cartSize: state.cart.length
});

export default connect(mapStateToProps)(Cart);
