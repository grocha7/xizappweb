import produce from "immer";
import { toast } from "react-toastify";

export default function cart(state = [], action) {
  console.log(state);
  switch (action.type) {
    case "ADD_TO_CART":
      return produce(state, draft => {
        const photoIndex = draft.findIndex(p => p.name === action.photo.name);

        if (photoIndex >= 0) {
          toast.error("Foto ja consta no carrinho");
        } else {
          draft.push(action.photo);
        }
      });
    case "REMOVE_FROM_CART":
      return produce(state, draft => {
        const photoIndex = draft.findIndex(p => p.id === action.id);

        if (photoIndex >= 0) {
          draft.splice(photoIndex, 1);
        }
      });
    default:
      return state;
  }
}
