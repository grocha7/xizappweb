import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  background: #fff;
  border-radius: 12px;
  width: 500px;
  justify-content: center;
  display: flex;

  @media (min-width: 1000px) {
    margin-left: 250px;
  }

  fieldset {
    border: none;
    width: 100%;
  }
`;

export const BTN = styled.input`
  background: #f7941d;
  color: #fff;
  border: 0;
  border-radius: 4px;
  padding: 12px 20px;
  text-transform: uppercase;
  transition: background 0.2s;
  display: block;
  justify-content: center;
  align-self: center;
  align-items: center;
  margin-left: 127px;
  margin-bottom: 10px;

  &:hover {
    background: ${darken(0.03, "#F7941D")};
  }
`;

export const List = styled.ul`
   li {
    display: flex;
    flex-direction: column;
    background: #fff;
    border-radius: 4px;
    padding: 20px;

    label{
    background: #f7941d;
    color: #fff;
    border: 0;
    width: 300px;
    border-radius: 4px;
    padding: 12px 20px;
    text-transform: uppercase;
    align-self: center;
    text-align: center;
    transition: background 0.2s;
    margin-bottom: 5px;
    }

    input {
    border: 1px solid #ddd;
    border-radius: 4px;
    color: #666;
    padding: 6px;
    width: 300px;
    align-self: center;
    display: flex;
    text-align: center;

`;
