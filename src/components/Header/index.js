import React from "react";

import { Container, Cart } from "./styles";
import { Link } from "react-router-dom";

import { connect } from "react-redux";

import { MdShoppingBasket } from "react-icons/md";

import logo from "../../assets/images/logo.svg";

function Header({ cartSize }) {
  return (
    <Container>
      <Link to="/">
        <img src={logo} alt="Xizapp" style={{ width: 60, height: 60 }} />
        <h1 style={{ color: "#FFF", fontSize: 20 }}>Xizapp</h1>
      </Link>

      <Cart to="/cart">
        <div>
          <strong>Meu carrinho</strong>
          <span>{cartSize} itens</span>
        </div>
        <MdShoppingBasket size={48} color="#FFF" />
      </Cart>
    </Container>
  );
}

export default connect(state => ({
  cartSize: state.cart.length
}))(Header);
